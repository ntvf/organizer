/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../scss/app.scss';

require('bootstrap');
require('jquery-ui-bundle/jquery-ui.min');
require('jquery-datetimepicker/build/jquery.datetimepicker.full.min');

require('fancybox/dist/js/jquery.fancybox');

$(document).ready(function () {
    $('.date-picker').datetimepicker({
        format: "d.m.Y H:i",
        formatTime: "H:i",
        step: 15,
        minDate: 0
    });

    $('.btn-delete').click(function (e) {
        if (!confirm('Are you sure want to delete this contact?')) {
            e.preventDefault();
            return false;
        }
    });

    var card = $('.btn-card').fancybox({
        type: 'ajax'
    });
});

