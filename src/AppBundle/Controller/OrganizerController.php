<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Repository\EventRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrganizerController extends Controller
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EventRepository $eventsRepo */
        $eventsRepo = $em->getRepository('AppBundle:Event');

        $eventCurrent = $eventsRepo->getCurrentEvent();

        $eventsCountUpcoming = $eventsRepo->countUpcomingForToday($eventCurrent);
        $eventsCountPast = $eventsRepo->countPastForToday();


        return $this->render('@App/event/index.html.twig', array(
                'current' => $eventCurrent,
                'upcoming' => $eventsCountUpcoming,
                'past' => $eventsCountPast,
            )
        );
    }

    /**
     * @Route("/list", name="event_list", methods={"GET"})
     */
    public function listAllAction(Request $request, PaginatorInterface $pagination)
    {
        /** @var EventRepository $eventRepo */
        $eventRepo = $this->getDoctrine()->getRepository(Event::class);
        $query = $eventRepo->getQueryList();

        $page = (int) $request->get('page');
        $page = $page > 0 ? $page : 1;

        $perPage = $this->getParameter('per_page');

        $events = $pagination->paginate($query, $page, $perPage,
            ['defaultSortFieldName' => 'e.date', 'defaultSortDirection' => 'asc']
        );

        return $this->render('@App/event/list.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * @Route("/list/upcoming", name="event_list_upcoming", methods={"GET"})
     */
    public function listUpcomingAction(Request $request, PaginatorInterface $pagination)
    {
        /** @var EventRepository $eventRepo */
        $eventRepo = $this->getDoctrine()->getRepository(Event::class);
        $query = $eventRepo->getQueryListUpcoming();

        $page = (int) $request->get('page');
        $page = $page > 0 ? $page : 1;

        $perPage = $this->getParameter('per_page');

        $events = $pagination->paginate($query, $page, $perPage,
            ['defaultSortFieldName' => 'e.date', 'defaultSortDirection' => 'asc']
        );

        return $this->render('@App/event/list.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * @Route("/list/today/upcoming", name="event_list_upcoming_today", methods={"GET"})
     */
    public function listUpcomingTodayAction(Request $request, PaginatorInterface $pagination)
    {
        /** @var EventRepository $eventRepo */
        $eventRepo = $this->getDoctrine()->getRepository(Event::class);
        $query = $eventRepo->queryListTodayUpcoming();

        $page = (int) $request->get('page');
        $page = $page > 0 ? $page : 1;

        $perPage = $this->getParameter('per_page');

        $events = $pagination->paginate($query, $page, $perPage,
            ['defaultSortFieldName' => 'e.date', 'defaultSortDirection' => 'desc']
        );

        return $this->render('@App/event/list.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * @Route("/list/today/past", name="event_list_past_today", methods={"GET"})
     */
    public function listPastTodayAction(Request $request, PaginatorInterface $pagination)
    {
        /** @var EventRepository $eventRepo */
        $eventRepo = $this->getDoctrine()->getRepository(Event::class);
        $query = $eventRepo->queryListTodayPast();

        $page = (int) $request->get('page');
        $page = $page > 0 ? $page : 1;

        $perPage = $this->getParameter('per_page');

        $events = $pagination->paginate($query, $page, $perPage,
            ['defaultSortFieldName' => 'e.date', 'defaultSortDirection' => 'desc']
        );

        return $this->render('@App/event/list.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * @Route("/list/past", name="event_list_past", methods={"GET"})
     */
    public function listPastAction(Request $request, PaginatorInterface $pagination)
    {
        /** @var EventRepository $eventRepo */
        $eventRepo = $this->getDoctrine()->getRepository(Event::class);
        $query = $eventRepo->getQueryListPast();

        $page = (int) $request->get('page');
        $page = $page > 0 ? $page : 1;

        $perPage = $this->getParameter('per_page');

        $events = $pagination->paginate($query, $page, $perPage,
            ['defaultSortFieldName' => 'e.date', 'defaultSortDirection' => 'desc']
        );

        return $this->render('@App/event/list.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * @Route("/list/completed", name="event_list_completed", methods={"GET"})
     */
    public function listCompletedAction(Request $request, PaginatorInterface $pagination)
    {
        /** @var EventRepository $eventRepo */
        $eventRepo = $this->getDoctrine()->getRepository(Event::class);
        $query = $eventRepo->getQueryListCompleted();

        $page = (int) $request->get('page');
        $page = $page > 0 ? $page : 1;

        $perPage = $this->getParameter('per_page');

        $events = $pagination->paginate($query, $page, $perPage,
            ['defaultSortFieldName' => 'e.date', 'defaultSortDirection' => 'asc']
        );

        return $this->render('@App/event/list.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * Creates a new event entity.
     *
     * @Route("/event/new", name="event_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $event = new Event();
        $form = $this->createForm('AppBundle\Form\EventType', $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();
            $this->addFlash('success', 'Event created');

            return $this->redirectToRoute('index', array('id' => $event->getId()));
        }

        return $this->render('@App/event/new.html.twig', array(
            'event' => $event,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/event/card/{id}", name="event_card")
     */
    public function cardAction(Event $event)
    {
        return $this->render('@App/event/card.html.twig', array(
            'event' => $event,
        ));
    }


    /**
     * @Route("/event/complete/{id}", name="event_complete")
     */
    public function completeAction(Request $request, Event $event)
    {
        $event->setCompleted(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($event);
        $em->flush();
        $this->addFlash('success', 'Event completed');

        return $this->redirectToRoute('index');
    }

    /**
     * Finds and displays a event entity.
     *
     * @Route("/event/{id}", name="event_show", methods={"GET"})
     */
    public function showAction(Event $event)
    {
        return $this->render('@App/event/show.html.twig', array(
            'event' => $event,
        ));
    }

    /**
     * Displays a form to edit an existing event entity.
     *
     * @Route("/event/{id}/edit", name="event_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Event $event)
    {
        $editForm = $this->createForm('AppBundle\Form\EventType', $event);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Event saved');

            return $this->redirectToRoute('event_list', array('id' => $event->getId()));
        }

        return $this->render('@App/event/edit.html.twig', array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a event entity.
     *
     * @Route("/event/delete/{id}", name="event_delete")
     */
    public function deleteAction(Request $request, Event $event)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($event);
        $em->flush();
        $this->addFlash('success', 'Event deleted');

        return $this->redirectToRoute('event_list');
    }

}
