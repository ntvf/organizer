<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Event;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

class TestDataFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // create 500 test records
        for ($rec = 1; $rec <= 1000; $rec++) {
            $dayOffset = rand(-30, 45);
            $minutesOffset = rand(-500, 500);

            $date = new \DateTime();
            $date->modify("{$dayOffset} days")->modify("{$minutesOffset} minutes");

            $title = "Event #{$rec} with {$dayOffset}/{$minutesOffset} offset";


            $description = '';

            // generate random base64 string for description strip test
            if (!($rec % 3)) {
                $bytes = random_bytes(rand(50, 1024));
                $description = preg_replace('/[ua10\/]{1}/', "\n", base64_encode($bytes));
            }

            $event = new Event();
            $event->setDate($date);
            $event->setTitle($title);
            $event->setDetails($description);
            $event->setCompleted(rand(0, 1));

            $manager->persist($event);
            $manager->flush();

            echo "Saving {$title}\n";
        }
    }
}